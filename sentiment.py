from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import glob
from pathlib import Path
analyzer = SentimentIntensityAnalyzer()

# List of files we want to use as inputs
files = glob.glob('comments/**/*.txt', recursive=True)

# Helper function for formatting value as percentage of total
def get_percent(val, total):
    p = (val / total) * 100
    return f'{p:.2f}%'

# Just printing the header for the csv
print('Channel,ID,Positive,Negative,Neutral,Total,Sentiment')
# For each file...
for file in files:
    # Open file and put into variable txt
    with open(file, 'r') as txt:
        # now reading lines into list comments
        comments = txt.readlines()
        p = Path(file)
        # initialize map for storing pos/neg/neutral vals
        map = {
            'positive': 0,
            'negative': 0,
            'neutral': 0
        }
        # using path above to get the ID from the filename (split on . to get just first part minus the .txt)
        id = p.name.split('.')[0]
        channel = p.parent.name
        for comment in comments:
            # Pass each line (one comment per line) into the VADER polarity thingy
            vs = analyzer.polarity_scores(comment)
            # Just calculating totals here, according to their site these are what each map to
            if vs['compound'] > 0:
                map['positive'] += 1
            if vs['compound'] < 0:
                map['negative'] += 1
            if vs['compound'] == 0.0:
                map['neutral'] += 1
        pos = map['positive']
        neg = map['negative']
        neu = map['neutral']
        # Making a cheeky inverted map. So the key is the value and the value is the key
        inv_map = {v: k for k, v in map.items()}
        # This way, we can get the max value of the initial map values (say, positive is the highest)
        # max value gives us the highest, so we use the inverse map to get the corresponding label
        sentiment = inv_map[max(map.values())]
        # saving total so we dont have to do len over and over
        total = len(comments)
        # Printing out the values in the order of the header above
        print(f'{channel},{id},{get_percent(pos, total)},{get_percent(neg, total)},{get_percent(neu, total)},{total},{sentiment}')