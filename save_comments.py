import yt
from pathlib import Path
# Making container dir for all comments
Path("comments").mkdir(parents=True, exist_ok=True)

# Reading all the videos into a list
with open("videos.txt", "r") as file:
    videos = file.readlines()

vidlist = []
# formatting list to be just the video ID
for v in videos:
    vid = v.strip().split('v=')[1]
    vidlist.append(vid)

# mapping each video to the channel name
vidmap = yt.get_details(vidlist)

# For each video, create a file in the comments dir/channelName containing the comments for that video
for v in vidlist:
    Path(f'comments/{vidmap[v]}').mkdir(parents=True, exist_ok=True)
    with open(f'comments/{vidmap[v]}/{v}.txt', 'x') as f:
        # Helper function to get all comments from the video id 'v' into a list 'comments'
        comments = yt.get_comments(v)
        # Then we write that bitch to a file
        f.write('\n'.join(comments))