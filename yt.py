from googleapiclient.discovery import build

# Your google api key
api_key='AIzaSyCoUsnL54ru0hgUzYkG4IRiu2mQUDSO_MY'
# flag for if you want the api to return the replies or not
# Currently ommitted because we didnt want negative replies on a positive comment to skew results
include_replies=False
# youtube api object
youtube = build('youtube', 'v3', developerKey=api_key)

# Looks up video ids to get list of channel names, makes map for ez lookups
def get_details(ids):
    vids = {}
    video_info = youtube.videos().list(id=ids, part="snippet").execute()
    for v in video_info['items']:
        id = v['id']
        snippet = v['snippet']
        vids[id] = snippet['channelTitle']
    return vids

# All flag is just for testing, put false if you want to run for the first 100 or so instead of like 20k+ 
def get_comments(video_id, all=True):
    comments = []
    replies = []
    parts = ['snippet']
    # Making inclusion of replies optional 
    if include_replies:
        parts.append('replies')
    # First page of comments lookup 
    video_response = youtube.commentThreads().list(part=list(parts), videoId=video_id, maxResults=100).execute()

    while video_response:
        for item in video_response['items']:
            comment = item['snippet']['topLevelComment']['snippet']['textDisplay']
            replycount = item['snippet']['totalReplyCount']

            if replycount > 0 and include_replies:
                for reply in item['replies']['comments']:
                    reply = reply['snippet']['textDisplay']
                    replies.append(reply)
            comments.append(comment)
            if include_replies:
                comments.extend(replies)
                replies = []
        if 'nextPageToken' in video_response and all:
            video_response = youtube.commentThreads().list(part=list(parts), videoId=video_id, pageToken=video_response['nextPageToken'], maxResults=100).execute()
        else:
            break

    return comments